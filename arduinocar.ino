#include <ServoTimer2.h>


#define echoPin 2 
#define trigPin 3 

#define MOTOR_LEFT_FRONT_PWM 10
#define MOTOR_LEFT_FRONT_AHEAD 12
#define MOTOR_LEFT_FRONT_BACK A1

#define MOTOR_LEFT_REAR_PWM 9
#define MOTOR_LEFT_REAR_AHEAD A3
#define MOTOR_LEFT_REAR_BACK A2

#define MOTOR_RIGHT_FRONT_PWM 6
#define MOTOR_RIGHT_FRONT_AHEAD 8 
#define MOTOR_RIGHT_FRONT_BACK 11

#define MOTOR_RIGHT_REAR_PWM 5
#define MOTOR_RIGHT_REAR_AHEAD A5
#define MOTOR_RIGHT_REAR_BACK A4




class Radar {
  public: 
  
    ServoTimer2 servo;
    int pin_distance, echo_pin, trig_pin;

    Radar (int echo_pn, int trig_pn){

      echo_pin = echo_pn;
      trig_pin = trig_pn;
    }

    void init(){
      pinMode(echo_pin, INPUT);
      pinMode(trig_pin, OUTPUT);
      servo.attach(4);
    }

    void turn(int degree ){
      degree = 180 - degree - 90;
      degree = int(750.0 / 90 * degree + 750); 
      servo.write(degree);
      delay(200);
    }

    void turn_right(){
      servo.write(2250);
      delay(500);
    }

   void turn_ahead(){
      servo.write(1500);
      delay(500);
    }

    void turn_left(){
      servo.write(500);
      delay(500);
    }

    int distance(){
      long duration;
      int dist;

      digitalWrite(trigPin, LOW);
      delayMicroseconds(2);
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(10);
      digitalWrite(trigPin, LOW);
  
      duration = pulseIn(echoPin, HIGH);
      dist = duration * 0.034 / 2; 
      
      return dist;
    }
    int get_left_distance(){
      turn(-90);
      delay(700);
      return distance();
    }

    int get_right_distance(){
      turn(90);
      delay(700);
      return distance();
    }
};


class Wheel {
  public:
              
    int pin_ahead, pin_back, pin_pwm;

    Wheel (int pn_ahead, int pn_back, int pn_pwm){
      pin_ahead = pn_ahead;
      pin_back = pn_back;
      pin_pwm = pn_pwm;
      // init();
    }
    
    void init(){
      pinMode(pin_pwm, OUTPUT);
      pinMode(pin_ahead, OUTPUT);
      pinMode(pin_back, OUTPUT);
    }

    void go_ahead(int speed){
      analogWrite(pin_pwm, speed);
      digitalWrite(pin_ahead, HIGH);
      digitalWrite(pin_back, LOW);    
    }

    void go_back(int speed){
      analogWrite(pin_pwm, speed);
      digitalWrite(pin_ahead, LOW);
      digitalWrite(pin_back, HIGH);  
    }

    void stop(){
      analogWrite(pin_pwm, 255);
      digitalWrite(pin_ahead, LOW);
      digitalWrite(pin_back, LOW);
    }
};

long duration; 
int distance;

Wheel wheel_front_left = Wheel(MOTOR_LEFT_FRONT_AHEAD, 
                               MOTOR_LEFT_FRONT_BACK,
                               MOTOR_LEFT_FRONT_PWM);

Wheel wheel_rear_left = Wheel(MOTOR_LEFT_REAR_AHEAD, 
                              MOTOR_LEFT_REAR_BACK,
                              MOTOR_LEFT_REAR_PWM);

Wheel wheel_front_right = Wheel(MOTOR_RIGHT_FRONT_AHEAD, 
                                MOTOR_RIGHT_FRONT_BACK,
                                MOTOR_RIGHT_FRONT_PWM);

Wheel wheel_rear_right = Wheel(MOTOR_RIGHT_REAR_AHEAD, 
                               MOTOR_RIGHT_REAR_BACK,
                               MOTOR_RIGHT_REAR_PWM);

Radar radar = Radar(echoPin, trigPin);


bool lr_measured  = false;

char Incoming_value = 0;



void setup() {

  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  Serial.begin(9600);
  

  wheel_rear_left.init();
  wheel_front_right.init();
  wheel_rear_right.init();
  wheel_front_left.init();
  
  radar.init();

  Serial.begin(9600);        
  pinMode(13, OUTPUT);
}


void loop(){

    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
  
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
  
    duration = pulseIn(echoPin, HIGH);
  
    distance = duration * 0.034 / 2;
   
    Serial.print("D: ");
    Serial.print(distance);
    Serial.println(" cm");  
  
    if(Serial.available() > 0){
        Incoming_value = Serial.read();    
        Serial.print(Incoming_value);        
        Serial.print("\n");
    
        if(Incoming_value == 'n'){    
            wheel_front_left.go_ahead(255);
            wheel_rear_left.go_ahead(255);
            wheel_front_right.go_ahead(255);
            wheel_rear_right.go_ahead(255);
    
        } else if (Incoming_value == 's') {
            wheel_front_left.go_back(150);
            wheel_rear_left.go_back(150);
            wheel_front_right.go_back(150);
            wheel_rear_right.go_back(150);
        
        } else if (Incoming_value == '0') {
            wheel_front_left.stop();
            wheel_rear_left.stop();
            wheel_front_right.stop();
            wheel_rear_right.stop();
    
        } else if (Incoming_value == 'w') {
            wheel_front_left.go_back(120);
            wheel_rear_left.go_back(120);
            wheel_front_right.go_ahead(120);
            wheel_rear_right.go_ahead(120);
           
        } else if (Incoming_value == 'e') {
            wheel_front_left.go_ahead(120);
            wheel_rear_left.go_ahead(120);
            wheel_front_right.go_back(120);
            wheel_rear_right.go_back(120);
            
        } else if (Incoming_value == '1'){    
            if (distance > 30) {
                radar.turn(0);
                wheel_front_left.go_ahead(255);
                wheel_rear_left.go_ahead(255);
                wheel_front_right.go_ahead(255);
                wheel_rear_right.go_ahead(255);
            }
                
        } else if (distance < 10) {
            wheel_front_left.go_back(150);
            wheel_rear_left.go_back(150);
            wheel_front_right.go_back(150);
            wheel_rear_right.go_back(150);
    
        } else  {
            wheel_front_left.stop();
            wheel_rear_left.stop();
            wheel_front_right.stop();
            wheel_rear_right.stop();
    
            float left_dist = radar.get_left_distance();
            float right_dist = radar.get_right_distance();
    
            if (left_dist > right_dist) {
                radar.turn(13);
                while (radar.distance() < 40){
                    wheel_front_left.go_back(120);
                    wheel_rear_left.go_back(120);
                    wheel_front_right.go_ahead(120);
                    wheel_rear_right.go_ahead(120);
                }  
          
            } else if (left_dist <= right_dist) {
                radar.turn(-13);
                while (radar.distance() < 40){
                    wheel_front_left.go_ahead(120);
                    wheel_rear_left.go_ahead(120);
                    wheel_front_right.go_back(120);
                    wheel_rear_right.go_back(120);
                }
            }
        }
    }
}
